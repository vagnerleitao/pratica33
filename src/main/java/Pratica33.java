import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        Matriz transp = orig.getTransposta();
        Matriz matrizSoma = new Matriz(3,2);
        double [][] m1 = matrizSoma.getMatriz();
        m1[0][0] = 0.5;
        m1[0][1] = 1.4;
        m1[1][0] = 1.5;
        m1[1][1] = 1.9;
        m1[2][0] = 3.0;
        m1[2][1] = 3.9;
        Matriz matriz3 = new Matriz(3,2);
        double [][] m3 = matriz3.getMatriz();
        m3[0][0] = 3.0;
        m3[0][1] = 2.0;
        m3[1][0] = 3.0;
        m3[1][1] = 3.0;
        m3[2][0] = 1.0;
        m3[2][1] = 2.0;
        Matriz matriz4 = new Matriz(2,3);
        double [][] m4 = matriz4.getMatriz();
        m4[0][0] = 3.0;
        m4[0][1] = 2.0;
        m4[0][2] = 6.0;
        m4[1][0] = 1.0;
        m4[1][1] = 2.0;
        m4[1][2] = 5.0;
        Matriz matrizResultado = new Matriz(3,2);
        double [][] m2 = matrizResultado.getMatriz();
        matrizResultado=matrizSoma.soma(orig);
        Matriz matrizResultadop = new Matriz(3,3);
        double [][] resultadop = matrizResultadop.getMatriz();
        matrizResultadop=matriz4.prod(matriz3);
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        System.out.println("Matriz Original 1 - Soma : "+orig);
        System.out.println("Matriz Original 2 - Soma : "+matrizSoma);
        System.out.println("Matriz Soma da Original 1 e Original 2 : "+matrizResultado);
        System.out.println("Matriz Original 3 - Produto : "+matriz3);
        System.out.println("Matriz Original 4 - Produto : "+matriz4);
        System.out.println("Matriz Produto da Original 3 e Original 4 : "+matrizResultadop);
    }
}
